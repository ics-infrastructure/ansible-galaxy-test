import json
import tower_cli


def generate_template_json():
    templates = tower_cli.get_resource("job_template")
    r = templates.list(all_pages=True)
    print(f"Number of job templates: {len(r['results'])}")
    with open("tower_templates.json", "w") as write_file:
        json.dump(r, write_file)

def generate_workflow_job_template_json():
    workflow_templates = tower_cli.get_resource("workflow")
    r = workflow_templates.list(all_pages=True)
    print(f"Number of workflow job templates: {len(r['results'])}")
    with open("workflow_job_templates.json", "w") as write_file:
        json.dump(r, write_file)


if __name__ == "__main__":
    generate_workflow_job_template_json()
    generate_template_json()
    
