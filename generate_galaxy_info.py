#!/usr/bin/env python
# Copyright (c) 2018 European Spallation Source ERIC
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
import click
import json
import os
import shlex
import sys
import subprocess
import yaml
from pathlib import Path


def run(cmd, cwd=None, abort_on_error=True):
    """Run the shell command and return the result"""
    args = shlex.split(cmd)
    try:
        result = subprocess.run(
            args, check=True, capture_output=True, text=True, cwd=cwd
        )
    except subprocess.CalledProcessError as e:
        if abort_on_error:
            click.echo(f"ERROR! Command {cmd} failed.", err=True)
            click.echo(e.stdout.strip())
            click.echo(
                e.stderr.replace(
                    "Welcome to ESS GitLab server!\n\nMake sure to upload your ssh key to your profile to allow ssh access.\n",
                    "",
                ).strip()
            )
            sys.exit(e.returncode)
        else:
            raise
    return result.stdout.strip()


def git_tag(cwd="."):
    """Return the most recent annotated tag"""
    try:
        return run("git describe --tags --abbrev=0", cwd=cwd, abort_on_error=False)
    except subprocess.CalledProcessError:
        click.echo(f"WARNING! No tag found in {cwd}", err=True)
        return ""


def parse_role(name, directory):
    p = Path(directory) / "meta" / "main.yml"
    if p.is_file():
        info = yaml.safe_load(p.read_bytes())
        # Make sure dependencies is a list of dict
        # and not list of roles (as string)
        dependencies = []
        for dependency in info.get("dependencies", []):
            if isinstance(dependency, dict):
                dependencies.append(dependency)
            else:
                dependencies.append({"role": dependency})
        info["dependencies"] = dependencies
    else:
        info = {"dependencies": [], "galaxy_info": None}
    tag = git_tag(directory)
    info["tag"] = tag
    info["name"] = name
    return info


def parse_playbook(name, directory):
    p = Path(directory) / "roles" / "requirements.yml"
    if p.is_file():
        requirements = yaml.safe_load(p.read_bytes()) or []
    else:
        requirements = []
    for requirement in requirements:
        if "name" not in requirement:
            requirement["name"] = requirement["src"].split("/")[-1].replace(".git", "")
    info = {"name": name, "requirements": requirements}
    return info


@click.command()
@click.option(
    "--pretty/--no-pretty",
    help="Pretty print the json output [default: False]",
    default=False,
)
@click.option(
    "--update/--no-update", help="Update all submodules [default: False]", default=False
)
@click.option(
    "--repos-dir",
    help="Ansible roles and playbooks directory [default: ./repos]",
    default="./repos",
)
@click.option(
    "--output",
    type=click.File("w"),
    help="Output filename [default: galaxy.json]",
    default="galaxy.json",
)
def cli(output, repos_dir, update, pretty):
    if update:
        click.echo("Updating all submodules...")
        run("git submodule update --remote")
    run("git submodule foreach --recursive 'git fetch --force --tags'")
    click.echo(f"Scanning repos directory {repos_dir}...")
    roles = []
    playbooks = []
    with os.scandir(repos_dir) as it:
        for entry in it:
            if entry.is_dir():
                if entry.name.startswith("ics-ans-role"):
                    roles.append(parse_role(entry.name, entry.path))
                else:
                    playbooks.append(parse_playbook(entry.name, entry.path))
    info = {"roles": roles, "playbooks": playbooks}
    click.echo(f"Creating {output.name}...")
    if pretty:
        json.dump(info, output, sort_keys=True, indent=4)
    else:
        json.dump(info, output)


if __name__ == "__main__":
    cli()
